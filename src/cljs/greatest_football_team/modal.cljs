(ns greatest-football-team.modal
  (:require
    [re-frame.core :as re]
    [greatest-football-team.utils :refer [<sub]]
    [greatest-football-team.subs :as subs]
    [greatest-football-team.events :as events]))

(defn modal-panel
  [{:keys [child size]}]
  [:div {:class "modal-wrapper"}
   [:div {:class    "modal-backdrop"
          :on-click (fn [event]
                      (do
                        (re/dispatch [::events/toggle-modal :settings nil])
                        (.preventDefault event)
                        (.stopPropagation event)))}]
   [:div {:class "modal-child"
          :style {:width (case size
                           :extra-small "15%"
                           :small "30%"
                           :large "70%"
                           :extra-large "85%"
                           "50%")}}
    child]])

(defn modal
  []
  (let [modal (<sub [::subs/modal :settings])]
    [:div
     (when (:visible? modal)
       [modal-panel (:settings modal)])]))

