(ns greatest-football-team.events
  (:require
    [cljs.reader]
    [re-frame.core :as re]
    [greatest-football-team.db]
    [greatest-football-team.utils :as utils]
    [ajax.core :as ajax]
    [day8.re-frame.http-fx]
    [day8.re-frame.async-flow-fx]))

(re/reg-fx
  :set-route
  (fn [{:keys [url]}]
    (utils/set-hash! url)))

(re/reg-fx
  :timeout
  (fn [{:keys [events milliseconds]}]
    (js/setTimeout (fn [] (run! #(re/dispatch %) events)) milliseconds)))

(defn fetch-sport-data-flow
  [options]
  {:first-dispatch [::fetch-sport-data (:sport options)]
   :rules          [{:when       :seen?
                     :events     ::fetch-sport-data-success
                     :dispatch-n [[::set-start-year (:start-year options)]
                                  [::set-end-year (:end-year options)]
                                  [::set-highlighted-team (:highlighted-team options)]
                                  [::set-sort-by (:sort-by options)]
                                  [::set-award-points-en-masse (:award-points options)]
                                  [::set-active-panel :home-panel]]
                     :halt?      true}]})

(re/reg-event-fx
  ::route-dispatch
  (fn [{:keys [db]} [_ route-info]]
    (let [route-id (:handler route-info)]
      (case route-id
        :home (if-let [options (utils/options-from-querystring)]
                {:async-flow (fetch-sport-data-flow options)}
                {:dispatch [::set-active-panel :home-panel]})
        :about {:dispatch [::set-active-panel :about-panel]}))))

(re/reg-event-db
  ::fetch-sport-data-success
  (fn [db [_ sport response]]
    (let [teams    (cljs.reader/read-string response)
          max-year (apply max (map utils/last-team-year teams))
          min-year (apply min (map :debut teams))]
      (assoc db
        :loading-sport? false
        :sport sport
        :teams teams
        :max-year max-year
        :min-year min-year
        :start-year (str min-year)
        :end-year (str max-year)))))

(re/reg-event-db
  ::fetch-sport-data-failure
  (fn [db [_ sport _]]
    (println "failure to retrieve " sport)
    (assoc db :loading-sport? false)))

(re/reg-event-fx
  ::fetch-sport-data
  (fn [{:keys [db]} [_ sport]]
    (when-not (= (:sport db) sport)
      (let [sport-str (name sport)]
        {:db         (assoc db :loading-sport? true
                               :sport sport)
         :http-xhrio {:method          :get
                      :uri             (str "data/" sport-str ".edn")
                      :response-format (ajax/raw-response-format)
                      :on-success      [::fetch-sport-data-success sport]
                      :on-failure      [::fetch-sport-data-failure sport]}}))))

(re/reg-event-fx
  ::initialize-app
  (fn [_ _]
    (let [default-db greatest-football-team.db/default-db]
      {:db       default-db
       :dispatch [::fetch-sport-data :baseball]})))

(re/reg-event-fx
  ::sport-change
  (fn [_ [_ sport]]
    {:set-route {:url (str "/sport/" sport)}}))

(re/reg-event-db
  ::set-active-panel
  (fn [db [_ active-panel]]
    (assoc db :active-panel active-panel)))

(defn x-or-min-max
  "Returns `x` if it is between `min` and `max` (inclusive). If less than `min`,
  then `min` is returned. If greater than `max`, then `max` is returned."
  [x min max]
  (cond
    (< x min) min
    (> x max) max
    :else x))

(defn update-year
  "Return `db` with the key `k` set to the `year` argument. If `year` is not
  between the allowed years then one of the allowed years is used."
  [db k year]
  (assoc db k (str (x-or-min-max year (:min-year db) (:max-year db)))))

(re/reg-event-db
  ::set-start-year
  (fn [db [_ start-year]]
    (assoc db :start-year start-year)))

(re/reg-event-db
  ::set-end-year
  (fn [db [_ end-year]]
    (assoc db :end-year end-year)))

(re/reg-event-db
  ::set-highlighted-team
  (fn [db [_ team-name]]
    (assoc db :highlighted-team team-name)))

(re/reg-event-db
  ::set-sort-by
  (fn [db [_ sort-by-keyword]]
    (assoc db :sort-by sort-by-keyword)))

(re/reg-event-db
  ::show-franchise-details
  (fn [db [_ franchise-name]]
    (let [same-franchise? (= franchise-name (:franchise-detail db))
          detail-visible? (not same-franchise?)]
      (-> db
          (assoc :detail-visible? detail-visible?)
          (assoc :franchise-detail (if same-franchise? nil franchise-name))))))

(re/reg-event-db
  ::dec-start-year
  (fn [db _]
    (let [new-start-year (-> db :start-year js/parseInt dec)]
      (update-year db :start-year new-start-year))))

(re/reg-event-db
  ::inc-start-year
  (fn [db _]
    (let [new-start-year (-> db :start-year js/parseInt inc)]
      (update-year db :start-year new-start-year))))

(re/reg-event-db
  ::dec-end-year
  (fn [db _]
    (let [new-end-year (-> db :end-year js/parseInt dec str)]
      (update-year db :end-year new-end-year))))

(re/reg-event-db
  ::inc-end-year
  (fn [db _]
    (let [new-end-year (-> db :end-year js/parseInt inc str)]
      (update-year db :end-year new-end-year))))

(re/reg-event-fx
  ::load-with-initial
  (fn [{:keys [db]} [_ start-year end-year highlight-team]]
    {:db         db
     :dispatch-n [[::set-active-panel :home-panel]
                  [::set-start-year start-year]
                  [::set-end-year end-year]
                  [::set-highlighted-team highlight-team]]}))

(re/reg-event-db
  ::set-share-button-text
  (fn [db [_ new-text]]
    (assoc db :share-button-text new-text)))

(re/reg-event-db
  ::remove-share-button-class
  (fn [db _]
    (dissoc db :share-button-class)))

(re/reg-event-fx
  ::copy-shareable-link
  (fn [{:keys [db]} _]
    (utils/copy-to-clipboard (utils/build-shareable-link db))
    {:db      (assoc db :share-button-text "Copied!"
                        :share-button-class "btn-success")
     :timeout {:events       [[::set-share-button-text "Share"]
                              [::remove-share-button-class]]
               :milliseconds 5000}}))

(re/reg-event-db
  ::set-award-points
  (fn [db [_ level points]]
    (assoc-in db [:award-points level] points)))

(re/reg-event-db
  ::set-award-points-en-masse
  (fn [db [_ points]]
    (assoc db :award-points points)))

(re/reg-event-db
  ::toggle-modal
  (fn [db [_ modal-id settings]]
    (-> db
        (update-in [:modals modal-id :visible?] not)
        (assoc-in [:modals modal-id :settings] settings))))
