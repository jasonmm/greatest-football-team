(ns greatest-football-team.db)

(def default-db
  {:name             "greatest-sports-team"
   :start-year       1966
   :end-year         2018

   :sort-by          :total-points
   :teams            []
   :detail-visible?  false
   :franchise-detail nil
   :highlighted-team nil

   ; must be nil so initial fetch is performed
   :sport            nil

   ; This map is a little bizarre because originally the points awarded for how
   ; far into the playoffs a team got was hardcoded into the source data. When
   ; the ability for the user to change the points was introduced the source
   ; data was not altered, so the values in the source data (8, 5, 3...) became
   ; indicators of which playoff round the team reached. That is why this map
   ; has keys with those values (:8, :5, :3...). The values of this map are the
   ; default points used by the program.
   ; todo: change the code that processes the source data to create EDN data with better indicators of how far into the playoffs a team reached.
   :award-points     {:8 8
                      :5 5
                      :3 3
                      :2 2
                      :1 1}})
