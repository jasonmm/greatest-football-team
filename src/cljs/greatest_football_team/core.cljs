(ns greatest-football-team.core
  (:require
   [reagent.dom :as rdom]
   [re-frame.core :as re-frame]
   [greatest-football-team.events :as events]
   [greatest-football-team.routes :as routes]
   [greatest-football-team.views :as views]
   [greatest-football-team.config :as config]))


(defn dev-setup
  []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root
  []
  (re-frame/clear-subscription-cache!)
  (rdom/render [views/main-panel]
               (.getElementById js/document "app")))

(defn ^:export init
  []
  (routes/start!)
  (re-frame/dispatch-sync [::events/initialize-app])
  (dev-setup)
  (mount-root))
