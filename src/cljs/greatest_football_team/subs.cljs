(ns greatest-football-team.subs
  (:require
   [re-frame.core :as re]
   [greatest-football-team.utils :as utils]))

(re/reg-sub
 ::name
 (fn [db]
   (:name db)))

(re/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))

(re/reg-sub
 ::get-start-year
 (fn [db _]
   (let [start-year (js/parseInt (:start-year db))]
     (if (js/isNaN start-year)
       ""
       start-year))))

(re/reg-sub
 ::get-end-year
 (fn [db _]
   (let [end-year (js/parseInt (:end-year db))]
     (if (js/isNaN end-year)
       ""
       end-year))))

(re/reg-sub
  ::get-award-points-for-level
  (fn [db [_ level]]
    (let [award-points (get-in db [:award-points level])
          award-points-int (js/parseInt award-points)]
      (if (js/isNaN award-points-int)
        ""
        award-points-int))))

(re/reg-sub
 ::get-highlighted-team
 (fn [db _]
   (:highlighted-team db)))

(re/reg-sub
 ::get-loading-sport?
 (fn [db _]
   (:loading-sport? db)))

(re/reg-sub
 ::get-sport
 (fn [db _]
   (:sport db)))

(re/reg-sub
 ::get-teams
 (fn [db _]
   (:teams db)))

(re/reg-sub
 ::get-team-names
 (fn [db _]
   (sort (map :franchise (:teams db)))))

(re/reg-sub
  ::get-sort-by
  (fn [db _]
    (or (:sort-by db)
        :total-points)))

(re/reg-sub
  ::get-award-points-map
  (fn [db _]
    ; Make sure every value of the map is an integer.
    (into {}
          (map (fn [[k v]] [k (utils/int-or-zero v)])
               (:award-points db)))))

(re/reg-sub
 ::get-teams-for-current-years
 :<- [::get-start-year]
 :<- [::get-end-year]
 :<- [::get-teams]
 :<- [::get-sort-by]
 :<- [::get-award-points-map]
 (fn [[start-year end-year teams sort-keyword award-points]]
   (->> teams
        (remove #(> (:debut %) end-year))
        (map #(utils/add-points-to-team % start-year end-year award-points))
        (sort-by sort-keyword)
        reverse)))

(re/reg-sub
  ::get-franchise-showing-details
  (fn [db _]
    (:franchise-detail db)))

(re/reg-sub
  ::get-share-button-text
  (fn [db _]
    (get db :share-button-text "Share")))

(re/reg-sub
  ::get-share-button-class
  (fn [db _]
    (get db :share-button-class "btn-secondary")))

(re/reg-sub
  ::modal
  (fn [db [_ modal-id]]
    (get-in db [:modals modal-id])))
