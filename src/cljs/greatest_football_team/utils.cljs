(ns greatest-football-team.utils
  (:require
    [re-frame.core]
    [clojure.string]
    [clojure.set :as set]
    [clojure.edn :as edn]))

(def <sub (comp deref re-frame.core/subscribe))

(defn set-hash!
  [new-hash]
  (set! (.-hash js/window.location) new-hash))

(defn target-value
  "A helper function for getting the event target's value. Used primarily to
  minimize the number of IDE warnings showing in the code."
  [event]
  (-> event .-target .-value))

(defn to-fixed
  "Converts `v` to a float with the given `precision`. Used primarily to
  minimize the number of IDE warnings showing in the code."
  [v precision]
  (.toFixed (js/parseFloat (str v)) precision))

(defn last-team-year
  "The last year the listed for the `team`."
  [team]
  (apply max (keys (:years team))))

(defn year-or-debut
  "Returns either the team's debut year or `year`, whichever is more recent."
  [team year]
  (if (> (:debut team) year)
    (:debut team)
    year))

(defn year-or-final
  "Returns either the teams last year or `year`, whichever is earlier."
  [team year]
  (let [last-year-for-team (last-team-year team)]
    (if (> last-year-for-team year)
      year
      last-year-for-team)))

(defn int-or-zero
  "Given a MapEntry return the MapEntry with the integer value of `v`. If `v`
  cannot be parsed as an integer then 0."
  [v]
  (let [v-int (js/parseInt v)]
    (if (js/isNaN v-int)
      0
      v-int)))

(defn award-points-for-level
  [award-points level]
  (int-or-zero (get award-points level)))

(defn placeholder->points
  [award-points placeholder]
  (award-points-for-level award-points (-> placeholder str keyword)))

(defn team-points
  "Returns a map with the points `team` has between `start-year` and `end-year`."
  [team start-year end-year award-points]
  (let [num-seasons (inc (- (year-or-final team end-year)
                            (year-or-debut team start-year)))
        total-pts   (reduce + (->> team
                                   :years
                                   (filter (fn [[yr _]] (<= start-year yr end-year)))
                                   vals
                                   (map (partial placeholder->points award-points))))
        pts-per-yr  (if (<= num-seasons 0) 0 (/ total-pts num-seasons))]
    {:total-points    total-pts
     :points-per-year pts-per-yr}))

(defn add-points-to-team
  "Adds the keys `:points` and `:points-per-year` to the `team` map. Calls the
  `team-points` function to get those values."
  [team start-year end-year award-points]
  (let [points-map (team-points team start-year end-year award-points)]
    (assoc team
      :total-points (:total-points points-map)
      :points-per-year (:points-per-year points-map))))

(defn valid-options?
  "Given a map describing the options in the UI determine if that map is valid
  to be used to set the options."
  [options]
  (= (set (keys options))
     #{:sport :start-year :end-year :highlighted-team :sort-by :award-points}))

(def querystring->db
  "Map the query-string parameter to the app-db key. The purpose of the short
  keys in the query-string is to keep the length of shareable URLs as short as
  possible."
  {:s    :sport
   :sy   :start-year
   :ey   :end-year
   :h    :highlighted-team
   :sort :sort-by
   :ap   :award-points})

(defn options-from-querystring
  "If the query contains valid UI options then return a map of those options."
  []
  (let [search-params (js/URLSearchParams. (.. js/window -location -search))
        state         (-> (.entries search-params)
                          js/Object.fromEntries
                          (js->clj :keywordize-keys true)
                          (set/rename-keys querystring->db)
                          (update :sport keyword)
                          (update :sort-by keyword)
                          (update :award-points edn/read-string))]
    (when (valid-options? state)
      state)))

(defn build-shareable-link
  "Given the app-db return a complete URL describing the current state of the
  UI options."
  [db]
  (let [base-url     (clojure.string/replace (.. js/window -location -href)
                                             (.. js/window -location -search)
                                             "")
        query-string (str "?s=" (name (:sport db))
                          "&sy=" (:start-year db)
                          "&ey=" (:end-year db)
                          "&h=" (js/encodeURIComponent (:highlighted-team db))
                          "&sort=" (name (get db :sort-by :total-points))
                          "&ap=" (js/encodeURIComponent (pr-str (:award-points db))))]
    (str base-url query-string)))

(defn copy-to-clipboard
  "https://gist.github.com/rotaliator/73daca2dc93c586122a0da57189ece13"
  [val]
  (let [el (js/document.createElement "textarea")]
    (set! (.-value el) val)
    (.appendChild js/document.body el)
    (.select el)
    (js/document.execCommand "copy")
    (.removeChild js/document.body el)))

