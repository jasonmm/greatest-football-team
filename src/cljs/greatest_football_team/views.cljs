(ns greatest-football-team.views
  (:require
    [re-frame.core :as re]
    [greatest-football-team.modal :as modal]
    [greatest-football-team.utils :as u :refer [<sub]]
    [greatest-football-team.subs :as subs]
    [greatest-football-team.events :as events]))

;; home

(defn team-details
  [team]
  [:table
   [:tbody
    [:tr
     [:td [:strong "Debut Year"]]
     [:td (:debut team)]]
    [:tr
     [:td [:strong "Final Year"]]
     [:td (:final team)]]
    [:tr
     [:td [:strong "Alternate Names"]]
     [:td (:alternate-names team)]]]])

(defn team-row
  [idx team]
  (let [highlighted-team            (<sub [::subs/get-highlighted-team])
        franchise-with-details-open (<sub [::subs/get-franchise-showing-details])]
    (vector :tr
            {:key   (:franchise team)
             :id    (:franchise team)
             :class (when (= highlighted-team (:franchise team)) "highlighted-team")}
            [:td (str (inc idx) ".")]
            [:td
             {:style    {:text-align :left :cursor :pointer}
              :on-click #(re/dispatch [::events/show-franchise-details (:franchise team)])}
             (:franchise team)
             (when (= (:franchise team) franchise-with-details-open)
               [team-details team])]
            [:td {:style {:text-align :center}} (:total-points team)]
            [:td {:style {:text-align :center}}
             (u/to-fixed (:points-per-year team) 3)])))

(defn team-table
  []
  (let [teams           (<sub [::subs/get-teams-for-current-years])
        sort-by-keyword (<sub [::subs/get-sort-by])]
    [:table.table.table-hover.table-sm.team-table
     [:thead
      [:tr
       [:th]
       [:th.pointer
        {:style    {:text-align :left}
         :on-click #(re/dispatch [::events/set-sort-by :franchise])}
        "Team"
        (when (= :franchise sort-by-keyword) "*")]
       [:th.pointer
        {:on-click #(re/dispatch [::events/set-sort-by :total-points])}
        "Points"
        (when (= :total-points sort-by-keyword) "*")]
       [:th.pointer
        {:on-click #(re/dispatch [::events/set-sort-by :points-per-year])}
        "Pts/Year"
        (when (= :points-per-year sort-by-keyword) "*")]]]
     (into [:tbody] (map-indexed team-row teams))]))

(defn setting-input
  [{:keys [title id]}]
  (let [award-points (<sub [::subs/get-award-points-for-level id])]
    [:div.form-group
     [:label {:for (name id)} title]
     [:input.form-control
      {:type      :number
       :value     award-points
       :id        id
       :on-change #(re/dispatch [::events/set-award-points id (u/target-value %)])}]]))

(defn settings-modal-body
  []
  [:div.settings-modal-body
   [:p.description "Changes are applied immediately. Tap the 'Close' button when finished."]
   [:h3.title "Award Points"]
   [setting-input {:title "Points For Winning The Championship"
                   :id    :8}]
   [setting-input {:title "Points For Winning The Conference Series"
                   :id    :5}]
   [setting-input {:title "Points For Winning The Divisional Series"
                   :id    :3}]
   [setting-input {:title "Points For Winning The Wild Card Series"
                   :id    :2}]
   [setting-input {:title "Points For Losing The Wild Card Series"
                   :id    :1}]
   [:div
    [:hr]
    [:button.btn.btn-sm.btn-secondary
     {:on-click #(re/dispatch [::events/toggle-modal :settings nil])}
     "Close"]]])

(defn year-form
  []
  (let [start-year (<sub [::subs/get-start-year])
        end-year   (<sub [::subs/get-end-year])]
    [:div
     [:div.row
      [:div.col-sm
       [:div.form-group
        [:label {:for "start-year"} "Start Year"]
        [:div.input-group.input-group
         [:div.input-group-prepend
          [:button.btn.btn-outline-secondary {:type     :button
                                              :on-click #(re/dispatch [::events/dec-start-year])} "< "]]
         [:input.form-control {:type       :number
                               :id         :start-year
                               :size       5
                               :value      start-year
                               :max-length 4
                               :on-change  #(re/dispatch [::events/set-start-year (u/target-value %)])}]
         [:div.input-group-append
          [:button.btn.btn-outline-secondary {:type     :button
                                              :on-click #(re/dispatch [::events/inc-start-year])} " >"]]]]]

      [:div.col-sm
       [:div.form-group
        [:label {:for "end-year"} "End Year"]
        [:div.input-group.input-group
         [:div.input-group-prepend
          [:button.btn.btn-outline-secondary {:type     :button
                                              :on-click #(re/dispatch [::events/dec-end-year])} "< "]]
         [:input.form-control {:type       :number
                               :id         :end-year
                               :size       5
                               :value      end-year
                               :max-length 4
                               :on-change  #(re/dispatch [::events/set-end-year (u/target-value %)])}]
         [:div.input-group-append
          [:button.btn.btn-outline-secondary {:type     :button
                                              :on-click #(re/dispatch [::events/inc-end-year])} " >"]]]]]]]))

(defn highlighted-team-chooser
  "Show a dropdown for selecting which team will be highlighted."
  []
  (let [highlighted-team (or (<sub [::subs/get-highlighted-team]) "")
        team-names       (<sub [::subs/get-team-names])]
    [:div.form-group
     [:label {:for "highlighted-team"} "Highlighted Team"]
     (into
       [:select.form-select {:value     highlighted-team
                             :id        :highlighted-team
                             :on-change #(re/dispatch [::events/set-highlighted-team (u/target-value %)])}]
       (map #(vector :option {:key % :value %} %)) (conj team-names ""))]))

(defn sport-chooser
  "Shows a dropdown for selecting which sport to display."
  [selected-sport]
  [:div.row
   [:div.col
    [:div.form-group
     [:label {:for "sport"} "Sport"]
     [:br]
     [:select.form-select {:value     selected-sport
                           :id        :sport
                           :on-change #(re/dispatch [::events/fetch-sport-data (u/target-value %)])}
      [:option {:key :baseball :value :baseball} "Baseball"]
      [:option {:key :basketball :value :basketball} "Basketball"]
      [:option {:key :football :value :football} "Football"]
      [:option {:key :canadian-football :value :canadian-football} "Canadian Football"]
      [:option {:key :hockey :value :hockey} "Hockey"]
      [:option {:key :major-league-soccer :value :major-league-soccer} "Major League Soccer"]]]]])


(defn home-panel []
  (let [sport           (<sub [::subs/get-sport])
        loading-sport?  (<sub [::subs/get-loading-sport?])
        share-btn-text  (<sub [::subs/get-share-button-text])
        share-btn-class (<sub [::subs/get-share-button-class])]
    [:div
     [modal/modal]
     [:button.shareable-link.btn.btn-sm
      {:class    share-btn-class
       :on-click #(re/dispatch [::events/copy-shareable-link])}
      share-btn-text]
     [:button.shareable-link.btn.btn-sm.btn-secondary
      {:on-click #(re/dispatch [::events/toggle-modal :settings {:child [settings-modal-body]
                                                                 :size  :extra-large}])}
      "Settings"]
     [:h1.title "Greatest Teams"]
     (when sport
       [:div [sport-chooser sport]
        (if loading-sport?
          [:div.loading-message "Loading..."]
          [:<>
           [:div [year-form]]
           [:div [highlighted-team-chooser]]
           [:hr]
           [:div [team-table]]])])]))


;; about

(defn about-panel []
  [:div
   [:h1 "This is the About Page."]

   [:div
    [:a {:href "#/"}
     "go to Home Page"]]])


;; main

(defn- panels [panel-name]
  (case panel-name
    :home-panel [home-panel]
    :about-panel [about-panel]
    [:div]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (re/subscribe [::subs/active-panel])]
    [show-panel @active-panel]))



; investigate using https://github.com/benhowell/re-frame-modal for a settings modal
; change query-string param names to be short to allow for many of them
; add ability to adjust weights (possibly in a modal)
;  - see README for which weights need to be adjusted
; add current weight setting to the share URL
