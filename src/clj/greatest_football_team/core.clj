(ns greatest-football-team.core
  "A little tool to convert source CSV data to EDN."
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.string :as str]))

(defn parse-int
  "Convert `i` to an integer, if it can be converted, otherwise return `i`
  unchanged."
  [i]
  (try
    (Integer/parseInt i)
    (catch Exception _
      (if (str/blank? i) 0 i))))

(defn resource->reader
  "Return a reader from the given resource filename."
  [filename]
  (io/reader (io/resource filename)))

(defn header->keyword
  "Return a keyword from the CSV header string."
  [header]
  (-> header
      str/lower-case
      (str/replace #" " "-")
      keyword))

(defn remove-non-franchise-years
  "Remove years when the franchise did not exist from the team map.
  A 'team map' represents a row from the CSV source."
  [team-map]
  (let [debut (:debut team-map)
        final (:final team-map)]
    (update team-map
            :years
            #(into {} (filter (fn [[year _]]
                                (<= debut year final))
                              %)))))

(defn kw->int
  "A helper function to keep from pasting this code everywhere."
  [kw]
  (parse-int (name kw)))

(defn int-keyword? [k] (int? (kw->int k)))
(defn string-keyword? [k] (string? (kw->int k)))

(defn add-years-key
  "Put all the years under a `:years` key."
  [team-map]
  (let [years    (->> team-map
                      (filter (comp int-keyword? first))
                      (into {})
                      (map (fn [[k v]] {(kw->int k) v}))
                      (apply merge))
        team-map (into {} (filter (comp string-keyword? first) team-map))]
    (assoc team-map :years years)))

(defn fix-alternate-names
  "The initial reading of the CSV data turns blanks into 0, because that makes
  sense for the year columns. This function:
   * turns the alternate names column from 0 back to blank
   * turns the final year column from 0 to the most recent franchise year"
  [team-map]
  (let [blank-alt-names? (= 0 (:alternate-names team-map))
        final-is-zero?   (= 0 (:final team-map))]
    (cond-> team-map
            blank-alt-names? (assoc :alternate-names "")
            final-is-zero? (assoc :final (apply max (keys (:years team-map)))))))

(defn add-total-points-key
  [team-map]
  (assoc team-map :total-points (apply + (vals (:years team-map)))))

(defn csv->edn
  [csv-filename]
  (let [reader   (io/reader csv-filename)
        csv-data (doall (csv/read-csv reader))
        headers  (->> csv-data
                      (take 1)
                      first
                      (map header->keyword))
        data     (->> csv-data
                      rest
                      (map #(map (fn [v] (parse-int v)) %))
                      (map #(zipmap headers %))
                      (map add-years-key)
                      (map fix-alternate-names)
                      (map remove-non-franchise-years))]
    (vec data)))


(comment

  (doall (csv/read-csv (io/reader "baseball.csv")))

  (def team
    (first (csv->edn "resources/source-data/canadian-football.csv")))
  (add-total-points-key team)


  #_())


(defn -main
  "A program to convert the source data from CSV to EDN.

  The CSV source data must have header text for every column.
  The column with the team name must have the header 'Team'.
  Each column for a year must have the full 4 digit year as the column header.
  There must be a column header called 'Debut' and the value in that column is
  the team's first year in the league.
  The column headers should be capitalized (as shown in the above sentences).
  For convenience in the UI the team's name should be just the most recent mascot.

  Usage: lein run <csv filename in 'resources/source-data' dir> <output filename in 'resources/public/data' dir>"
  [& args]
  (let [input-filename  (first args)
        output-filename (second args)
        edn-data        (doall (csv->edn (io/resource (str "source-data/" input-filename))))]
    (spit (io/resource (str "public/data/" output-filename)) edn-data)))
